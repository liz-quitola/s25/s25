db.course_bookings.insertMany([
	{"courseId": "C001", "studentId": "S004", "isCompleted": true},
	{"courseId": "C002", "studentId": "S001", "isCompleted": false},
	{"courseId": "C001", "studentId": "S003", "isCompleted": true},
	{"courseId": "C003", "studentId": "S002", "isCompleted": false},
	{"courseId": "C001", "studentId": "S002", "isCompleted": true},
	{"courseId": "C004", "studentId": "S003", "isCompleted": false},
	{"courseId": "C002", "studentId": "S004", "isCompleted": true},
	{"courseId": "C003", "studentId": "S007", "isCompleted": false},
	{"courseId": "C001", "studentId": "S005", "isCompleted": true},
	{"courseId": "C004", "studentId": "S008", "isCompleted": false},
	{"courseId": "C001", "studentId": "S013", "isCompleted": true}
]);

/*
	Aggregation in MongoDB
		This is the act or process of generating manipulated data and perform operations to create filtered results that helps in analyzing data.

	Syntax:
		db.collectionName.aggregate([
				{stage 1},
				{stage 2},
				{stage 3}
		]);
*/

/*
	$group 
		Syntax:
			{$group: {_id: <id>, fieldResult: "valueResult" }}
*/

db.course_bookings.aggregate([
	{
		$group: { _id: null, count: { $sum: 1 } }
	}
]);

/*
	Syntax:
		{$match: {field: value}}
*/

db.course_bookings.aggregate([
	{
		$match: { "isCompleted": true }
	},
	{
		$group: { _id: "$courseId", total: {$sum: 1 }}
	}
]);

/*
	$project
		Syntax: 
			{$project: {field: 0 or 1}}	
*/

db.course_bookings.aggregate([
	{ $match: {"isCompleted": true} },
	{ $project: { "studentId": } }
]);

/*
	$sort 
		Syntax: 
			{$sort: {field: 1/-1}}
*/
db.course_bookings.aggregate([
	{ $match: {"isCompleted": true}},
	{ $sort: {"courseId": -1 }}
]);


db.course_bookings.aggregate([
	{ $match: {"isCompleted": true}	},
	{ $sort: {"studentId": 1 } },
	{ $project: { "courseId": 1, "studentId": 1, "_id": 0 }}
]);

/*
	Mini Activity:
		1. Count the completed courses of student s013

		2. Sort the courseId in descending order while studentId in ascending order.
*/

db.course_bookings.aggregate([
		{ $match: { "studentId": "S013", "isCompleted": true }},
		{ $group: {_id: null, totalNumberOfCompletedCourse: {$sum: 1 } } }
]);

db.course_bookings.aggregate([
		{ $match: { "studentId": "S013", "isCompleted": true }},
		{ $group: {_id: "StudentId", totalNumberOfCompletedCourse: {$sum: 1 } } }
]);

/*
	Syntax:
		{ $count: <nameOfTheOutputFieldWhichHasItsCountAsItsValue> }
*/

db.course_bookings.aggregate([
		{ $match: { "studentId": "S013", "isCompleted": true }},
		{ $count: "totalNumberOfCompletedCourse" }
]);


db.course_bookings.aggregate([
		{ $sort: {"courseId": -1, "studentId": 1 }}
	]);

db.orders.insertMany([
		{
			"cust_Id": "A123",
			"amount": 500,
			"status": "A"
		},
		{
			"cust_Id": "A123",
			"amount": 250,
			"status": "A"
		},
		{
			"cust_Id": "B212",
			"amount": 200,
			"status": "A"
		},
		{
			"cust_Id": "B212",
			"amount": 200,
			"status": "D"
		}
]);

db.orders.aggregate([
		{ $match: {"status": "A"}},
		{ $group: {_id: "$cust_Id", maxAmount: { $max: "$amount" }}}
	]);

/*
	
	$sum operator - will total the values
	$max operator - will show you the highest value
	$min operator - will show your the lowest value
	$avg operator - will get the average value

*/
